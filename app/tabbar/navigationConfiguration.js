/**
 * Created by lijizhuang on 2017/7/22.
 */
'use strict';

import { TabNavigator, NavigationActions } from 'react-navigation'
// Tab-Navigators
import SearchNavigation from '../components/home/SearchNavigation';
import HotNavigation from '../components/hot/HotNavigation';
import ArticleNavigation from '../components/article/ArticleNavigation';

var Platform = require('Platform');

const routeConfiguration = {
    SearchNavigation: {screen: SearchNavigation},
    HotNavigation: {screen: HotNavigation},
    ArticleNavigation: {screen: ArticleNavigation}
};

const tabBarConfiguration = {
    //animationEnabled: false,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    backBehavior: 'none',
    lazy: false,
    //...other configs
    tabBarOptions: {
        // tint color is passed to text and icons (if enabled) on the tab bar
        activeTintColor: '#1C9DD7',
        inactiveTintColor: '#4d4d4d',
        showIcon: true,

        indicatorStyle: {height: 0},
        style: {
            backgroundColor: '#fff', // TabBar 背景色
            height: 49
        },
        iconStyle: {
            marginTop:10
        },
        tabStyle:{
            backgroundColor: '#FCFCFC',
            height: 49,
            borderTopWidth: 0.3,
            borderColor: '#4d4d4d' //安卓默认没有顶部的分割线，需和iOS保持一致
        },
        labelStyle: {
            fontSize: 12, // 文字大小
            marginTop: 4
        },

//// background color is for the tab component
//        activeBackgroundColor: 'blue',
//        inactiveBackgroundColor: 'white',
    }
};

export const TabBar = TabNavigator(routeConfiguration, tabBarConfiguration)

export const tabBarReducer = (state, action) => {
    if (action.type === 'JUMP_TO_TAB') {
        return {...state, index: 0}
    } else {
        return TabBar.router.getStateForAction(action, state)
    }
};
