/**
 * Created by lijizhuang on 2017/7/7.
 */

'use strict';
import address from './address';
import producer from './producer';

export default class Channel {

    constructor(options) {
        this.options = options
    }

    getSearchResult(keywords, page, rows, type = 'all') {
        const url = address.searchKeywords(keywords, page, rows, type);
        return window.fetch(url)
            .then(res => res.json())
            .then(data => {
                const items = producer.searchGeneral(data);
                return items;
            })
    }

    getBaiduSuggestion(keyword) {
        const url = address.autocomplete(keyword);
        return window.fetch(url)
            .then(res => res.json())
            .then(data => {
                return data[1];
            });

        //return window.fetch(url)
        //    .then(res=> {
        //        temp = res.text().replace("window.baidu.sug(", "");
        //        temp = temp.replace(");");
        //        return temp;
        //    })
        //    .then(data=>data.json())
        //    .then(result=> {
        //        return result['s'];
        //    })
    }

    getHotKeywords(){
        const  url = address.hotKeywords();
        return window.fetch(url)
            .then(res=>res.json())
            .then(data=>{
                return data
            });
    }

}