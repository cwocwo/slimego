/**
 * Created by lijizhuang on 2017/7/7.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform
} from 'react-native';

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reducers from './reducers'

import logger from './middleware/logger'
import thunk from 'redux-thunk'

import TabBarNavigation from './tabbar/views/TabBarNavigation'
//import App from './containers/App'

const createStoreWithMW = applyMiddleware(logger, thunk)(createStore); //中间件可以增强默认的dispatch函数
const store = createStoreWithMW(reducers); //二. 创建store,createStore 函数接收两个参数，(reducer, [initialState])，
// reducer 毋庸置疑，他需要从 store 获取 state，以及连接到 reducer 交互。
//initialState 是可以自定义的一个初始化 state，可选参数。
import codePush from 'react-native-code-push';
import SplashScreen from 'react-native-splash-screen';

export default class Root extends Component {
    componentDidMount() {
        //if (Platform.OS === 'android')
        SplashScreen.hide(); //解决安卓加载offline bundle出现白屏的情况

        codePush.sync();
    }

    render() {
        {/* 一.入口:从 react-redux 中获取了一个 Provider 组件，我们把它渲染到应用的最外层。
         他需要一个属性 store ，他把这个 store 放在context里，给App(connect)用。*/
        }
        return (
            <Provider store={store}>
                <TabBarNavigation />
            </Provider>
        )
    }
}
