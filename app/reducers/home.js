/**
 * Created by lijizhuang on 2017/7/8.
 */
'use strict';

import createReducer from '../utils/create-reducer'

import {HOME} from '../constants'

/* search result */
const initialState = {
    actionType:'',
    items:[],
    suggestions: [],
    cateTabIndex: -1
};

const actionHandler = {
    [HOME.SEARCH]: (state, action) => {
        return Object.assign({}, state, {
            actionType:action.type,
            items: action.data
        })
    },

    [HOME.SUGGEST]: (state, action) => {
        return Object.assign({}, state, {
            actionType:action.type,
            suggestions: action.data
        })
    },
};

export default createReducer(initialState, actionHandler);