/**
 * Created by lijizhuang on 2017/7/6.
 */
'use strict';

import createReducer from '../utils/create-reducer'

import {APP} from '../constants'

/* First navigator of each tab named [tab]Index */
const initialState = {
    tab: 'home',
    navigator: 'searchIndex'
};

const actionHandler = {
    [APP.TAB]: (state, action) => {
        return Object.assign({}, state, {
            tab: action.data,
            navigator: action.data + 'Index'
        })
    },

    [APP.NAVIGATION]: (state, action) => {
        return Object.assign({}, state, {
            navigator: action.data
        })
    }
};

export default createReducer(initialState, actionHandler)