/**
 * Created by lijizhuang on 2017/7/14.
 */

import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Keyboard,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    FlatList,
    InteractionManager,
    NativeModules,
} from 'react-native';

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');
const dismissKeyboard = require('dismissKeyboard');

import SuggestionListView from './SuggestionListView';
// Icon
import Icon from 'react-native-vector-icons/FontAwesome'
import ScrollableTabView, {DefaultTabBar,ScrollableTabBar} from '../../react-native-scrollable-tab-view';
import SearchList from './SearchList';

import {NavigationActions} from 'react-navigation';
var Platform = require('Platform');
var UMNative = (Platform.OS === 'ios' ? NativeModules.UMNative : NativeModules.UmengNativeModule);

var currentTabIndex = 0;
export default class SearchResults extends Component {
    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            selectedKeyword: params.keyword,//搜索框输入和选择的关键词
            showSuggest: false
        }
    }

    componentDidMount() {
        const {params} = this.props.navigation.state;
        this.setState({
            selectedKeyword: params.keyword
        });
    }

    componentWillUnmount() {
        this.resetListviewsToFirstLoad();

        var key;
        if (this.props.navigation.state.routeName === "SearchResultsFromHots")
            key = "HotNavigation";
        else
            key = "SearchNavigation";

        //显示tabbar
        const setParamsAction = NavigationActions.setParams({
            params: {hideTabBar: false},
            key: key,
        });
        this.props.navigation.dispatch(setParamsAction);
    }

    componentWillReceiveProps(props) {
        const {home} = this.props.screenProps;
    }


    render() {
        const {home, homeActions, category, cateActions} = this.props.screenProps;
        //const { params } = this.props.navigation.state;
        const _this = this;
        return (
            //导航栏
            <View style={styles.container}>
                <View style={styles.navView}>
                    <TouchableOpacity style={[styles.backBtn, {}]} onPress={this.doBack.bind(this)}>
                        <Icon style={styles.backIcon} name={ 'chevron-left' }/>
                    </TouchableOpacity>
                    {/*搜索框和按钮*/}
                    <View style={{flexDirection:'row'}}>
                        <TextInput ref="searchTB"
                                   returnKeyType="search"
                                   placeholder="搜索关键词，如：iOS视频教程"
                                   style={styles.searchInput}
                                   underlineColorAndroid='transparent'
                                   onSubmitEditing={this.doSearch.bind(this)}
                                   onChangeText={this.getValue.bind(this)}
                                   value={this.state.selectedKeyword}
                        />
                        <TouchableHighlight style={styles.searchBtn} onPress={this.doSearch.bind(this)}
                                            underlayColor="white">
                            <Image
                                style={{width:24, height:24}}
                                source={{uri: 'search_btn'}}/>
                        </TouchableHighlight>
                    </View>
                </View>
                {/*搜索关键词提示框*/}
                { this.state.showSuggest &&
                <View style={styles.suggestionView}>
                    <SuggestionListView onSelectKeyword={this.selectKeyword.bind(this)}
                                        style={styles.suggestionListView} home={home} actions={homeActions}/>
                </View>
                }
                {/*分类tab*/}
                <ScrollableTabView locked={true} renderTabBar={(props) => <ScrollableTabBar/>} onChangeTab={(obj) => {
                    //console.log('index:' + obj.i);
                    //console.log('重新加载');
                    InteractionManager.runAfterInteractions(() => {
                    currentTabIndex = obj.i;
                    switch(obj.i){
                        case 0:this.refs.allList && this.refs.allList.forceRefresh();break;
                        case 1:this.refs.videoList && this.refs.videoList.forceRefresh();break;
                        case 2:this.refs.appList && this.refs.appList.forceRefresh();break;
                        case 3:this.refs.docList && this.refs.docList.forceRefresh();break;
                        case 4:this.refs.musicList && this.refs.musicList.forceRefresh();break;
                        case 5:this.refs.imageList && this.refs.imageList.forceRefresh();break;
                        case 6:this.refs.otherList && this.refs.otherList.forceRefresh();break;
                    }
                    });
                    //   cateActions.switchTopTab(obj.i);
                    }
                  }
                                   initialPage={0}
                                   tabBarUnderlineColor='#6ec7eb'
                                   tabBarActiveTextColor='#6ec7eb'
                                   tabBarInactiveTextColor='#4d4d4d'
                                   tabBarTextStyle={{fontSize: 16}}
                                   underlineStyle={{color: '#6ec7eb'}}
                >
                    {/*<Text tabLabel='全部'/>*/}
                    <SearchList ref="allList" category={category} navigation={this.props.navigation} type='all'
                                tabLabel='全部'></SearchList>
                    <SearchList ref="videoList" category={category} navigation={this.props.navigation} type='video'
                                tabLabel='视频'></SearchList>
                    <SearchList ref="appList" category={category} navigation={this.props.navigation} type='app'
                                tabLabel='应用'></SearchList>
                    <SearchList ref="docList" category={category} navigation={this.props.navigation} type='doc'
                                tabLabel='文档'></SearchList>
                    <SearchList ref="musicList" category={category} navigation={this.props.navigation} type='music'
                                tabLabel='音乐'></SearchList>
                    <SearchList ref="imageList" category={category} navigation={this.props.navigation} type='image'
                                tabLabel='图片'></SearchList>
                    <SearchList ref="otherList" category={category} navigation={this.props.navigation} type='other'
                                tabLabel='其它'></SearchList>
                </ScrollableTabView>
            </View>
        );
    }

    doBack() {
        this.props.navigation.goBack();
        this.resetListviewsToFirstLoad();

        var key;
        if (this.props.navigation.state.routeName === "SearchResultsFromHots")
            key = "HotNavigation";
        else
            key = "SearchNavigation";

        //显示tabbar
        const setParamsAction = NavigationActions.setParams({
            params: {hideTabBar: false},
            key: key,
        });
        this.props.navigation.dispatch(setParamsAction);
    }

    resetListviewsToFirstLoad() {
        //重置页面第一次载入的flag，下次push跳转时可以出发第一个tab的刷新
        {
            this.refs.allList && this.refs.allList.resetFirstLoader();
        }
        {
            this.refs.videoList && this.refs.videoList.resetFirstLoader();
        }
        {
            this.refs.appList && this.refs.appList.resetFirstLoader();
        }
        {
            this.refs.docList && this.refs.docList.resetFirstLoader();
        }
        {
            this.refs.musicList && this.refs.musicList.resetFirstLoader();
        }
        {
            this.refs.imageList && this.refs.imageList.resetFirstLoader();
        }
        {
            this.refs.otherList && this.refs.otherList.resetFirstLoader();
        }
    }

    getValue(text) {
        const {homeActions, cateActions} = this.props.screenProps;
        var value = text;
        if (value.trim().length > 0) {
            homeActions.getBaiduSuggestion(value);
        }
        else {
            homeActions.clearBaiduSuggestion();
        }

        this.setState({
            showSuggest: true,
            selectedKeyword: text
        });

        cateActions.syncKeyword(text);
    }

    selectKeyword(keyword) {
        Keyboard.dismiss();

        this.setState({
            selectedKeyword: keyword,
            showSuggest: false
        });

        //this.props.navigation.navigate('SearchResults', { keyword: keyword });
        const {cateActions} = this.props.screenProps;
        cateActions.syncKeyword(keyword);
    }

    doSearch(event) {
        Keyboard.dismiss();
        var skeyword = this.state.selectedKeyword.trim();
        if (skeyword.length > 0) {
            this.setState({
                showSuggest: false
            });

            UMNative.onEventWithParameters("100001", {keyword: this.state.selectedKeyword});
        }

        //this.refs.stv.goToPage(0);
        switch (currentTabIndex) {
            case 0:
                this.refs.allList && this.refs.allList.forceRefresh();
                break;
            case 1:
                this.refs.videoList && this.refs.videoList.forceRefresh();
                break;
            case 2:
                this.refs.appList && this.refs.appList.forceRefresh();
                break;
            case 3:
                this.refs.docList && this.refs.docList.forceRefresh();
                break;
            case 4:
                this.refs.musicList && this.refs.musicList.forceRefresh();
                break;
            case 5:
                this.refs.imageList && this.refs.imageList.forceRefresh();
                break;
            case 6:
                this.refs.otherList && this.refs.otherList.forceRefresh();
                break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    navView: {
        flexDirection: 'row',
        height: 64,
        backgroundColor: '#1C9DD7'
    },

    backBtn: {
        marginTop: 25,
        marginLeft: 15
    },

    backIcon: {
        fontSize: 23,
        color: '#fff'
    },

    searchInput: {
        width: 0.8 * width,
        height: 33,
        backgroundColor: 'white',
        marginTop: 20,
        marginLeft: 20,

        // 设置圆角
        borderRadius: 5,

        paddingLeft: 5,
        // 内左边距
        paddingRight: 28,
        paddingTop: 0,
        paddingBottom: 0,
        borderColor: '#227db6',
        borderWidth: 1,
        fontSize: 12
    },

    searchBtn: {
        position: 'absolute',
        right: 8,
        top: 25
    },

    suggestionView: {
        alignItems: 'flex-start',
        zIndex: 999,
        width: 0.8 * width,
        marginLeft: 51,
        marginTop: 55,
        position: 'absolute'
    },

    suggestionListView: {
        position: 'absolute'
    },
});