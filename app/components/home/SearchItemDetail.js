/**
 * Created by lijizhuang on 2017/8/2.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Keyboard,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    WebView
} from 'react-native';

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/FontAwesome'
import address from '../../channel/address';

export default class SearchItemDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            canGoBack: false
        }
    }

    render() {
        const {params} = this.props.navigation.state;
        var rowData = params.rowData;
        const url = address.jumpResource(rowData.uk, rowData.shareId);
        return (
            <View style={styles.container}>
                <View style={styles.navView}>
                    <TouchableOpacity style={[styles.backBtn, {}]} onPress={this.doBack.bind(this)}>
                        <Icon style={styles.backIcon} name={ 'chevron-left' }/>
                    </TouchableOpacity>
                    {/*搜索框和按钮*/}
                    <View style={{width:0.8*width}}>
                        <Text numberOfLines={1} style={styles.name}>{rowData.name}</Text>
                    </View>
                </View>
                <WebView
                    ref='webView'
                    style={{width:width,height:height-64,backgroundColor:'white'}}
                    source={{uri:url,method: 'GET'}}
                    startInLoadingState={true}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    scalesPageToFit={false}
                    scalesPageToFit={false}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                    onShouldStartLoadWithRequest={(e) => {
                        var scheme = e.url.split('://')[0]
                        if(scheme === 'http' || scheme === 'https'){
                        return true
                        }
                        return false
                    }}
                />
                <TouchableOpacity style={[styles.wvBackBtn, {}]} onPress={this.doWvBack.bind(this)}>
                    <Icon style={styles.wvBackButton} name={ 'arrow-circle-left' }/>
                </TouchableOpacity>
            </View>
        );
    }

    doBack() {
        //var webView = this.refs.webView;
        //if (this.state.canGoBack) {
        //    webView.goBack();
        //}
        //else {
            this.props.navigation.goBack();
        //}
    }

    doWvBack(){
        var webView = this.refs.webView;
        webView.goBack();
    }

    onNavigationStateChange(navState) {
        //该方法有bug，单页应用的内页跳转会导致canGoBack状态不正确
        var {canGoBack} = navState;
        this.setState({
            canGoBack
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    navView: {
        flexDirection: 'row',
        height: 64,
        backgroundColor: '#1C9DD7'
    },

    backBtn: {
        marginTop: 25,
        marginLeft: 15
    },

    backIcon: {
        fontSize: 23,
        color: '#fff'
    },

    name: {
        fontSize: 17,
        color: 'white',
        marginTop: 27,
        marginLeft: 15,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },

    wvBackBtn:{
        position: 'absolute',
        marginTop: height * 0.45,
        marginLeft: 15
    },

    wvBackButton:{
        fontSize: 30,
        color: '#1C9DD7',
        backgroundColor: 'transparent'
    }
});