/**
 * Created by lijizhuang on 2017/7/9.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ListView,
    TouchableHighlight
} from 'react-native';

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');

export default class SuggestionListView extends Component {
    // 初始化模拟数据
    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(props) {
        const {home} = props;
        if (home.suggestions) {
            const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

            this.setState({
                dataSource: ds.cloneWithRows(home.suggestions)
            });
        }
    }

    componentDidMount() {
        const {actions} = this.props;
    }

    render() {
        const {home} = this.props;
        if (this.state && this.state.dataSource && home.suggestions) {
            return (
                <View style={styles.outsideView}>
                    <ListView style={styles.listView} scrollEnabled={true} bounces={false} enableEmptySections={true}
                              dataSource={this.state.dataSource}
                              renderRow={(rowData) => <TouchableHighlight onPress={this.pickSuggestion.bind(this, rowData)}><View style={styles.insideView}><Text style={{fontSize: 16, marginLeft:5}}>{rowData}</Text></View></TouchableHighlight>}
                    />
                </View>
            );
        }
        else {
            return (
                <View style={{paddingTop: 2}}></View>
            );
        }
    }

    pickSuggestion(rowData) {
        //传递选中的关键字传递给父容器的TextInput
        this.props.onSelectKeyword(rowData);
    }
}

const styles = StyleSheet.create({
    outsideView: {
        paddingTop: 2,
        //width: 0.85 * width,
        alignSelf: 'stretch',
        zIndex: 999
    },

    insideView: {
        height: 30,
        paddingTop: 5,
        backgroundColor: '#fff',
        borderBottomColor: '#e8e8e8',
        borderBottomWidth: 0.5
    },

    listView: {
        height: height <= 480 ? 150 : 0.35 * height,
    }
});
