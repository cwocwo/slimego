/**
 * Created by lijizhuang on 2017/7/23.
 */
'use strict';

import { StackNavigator } from 'react-navigation'
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStackStyleInterpolator'

// Screens
import SearchIndex from './SearchIndex'
import SearchResults from './SearchResults'
import SearchItemDetail from './SearchItemDetail'

const routeConfiguration = {
    SearchIndex: {
        screen: SearchIndex,
    },
    SearchResults: {screen: SearchResults},
    SearchItemDetail: {screen: SearchItemDetail}
};

// going to disable the header for now
const stackNavigatorConfiguration = {
    headerMode: 'none',
    initialRouteName: 'SearchIndex',
    mode: 'card',
    transitionConfig: () => ({
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps;
            const { index } = scene;

            const translateX = position.interpolate({
                inputRange: [ index - 1, index, index + 1 ],
                outputRange: [ layout.initWidth, 0, 0 ]
            });

            const opacity = position.interpolate({
                inputRange: [ index - 1, index - 0.99, index, index + 0.99, index + 1 ],
                outputRange: [ 0, 1, 1, 0, 0 ]
            });

            return { opacity, transform: [{ translateX }] };
        }
    }) //安卓和iOS push transition保持一致
};

export const NavigatorSearch = StackNavigator(routeConfiguration, stackNavigatorConfiguration);
