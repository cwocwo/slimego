/**
 * Created by lijizhuang on 2017/7/24.
 */
'use strict';
import { StackNavigator } from 'react-navigation'

// Screens
import ArticleIndex from './ArticleIndex'

const routeConfiguration = {
    ArticleIndex: { screen: ArticleIndex },
};

// going to disable the header for now
const stackNavigatorConfiguration = {
    headerMode: 'none',
    initialRoute: 'ArticleIndex'
};

export const NavigatorArticle = StackNavigator(routeConfiguration,stackNavigatorConfiguration);