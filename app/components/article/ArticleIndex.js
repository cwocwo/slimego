/**
 * Created by lijizhuang on 2017/8/2.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Keyboard,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    WebView
} from 'react-native';

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');

import Icon from 'react-native-vector-icons/FontAwesome'
import address from '../../channel/address';

export default class SearchItemDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            canGoBack: false
        }
    }

    render() {
        const url = address.articleList();
        return (
            <View style={styles.container}>
                <WebView
                    ref='webView'
                    style={{width:width,height:height,backgroundColor:'white'}}
                    source={{uri:url,method: 'GET'}}
                    startInLoadingState={true}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    scalesPageToFit={false}
                    onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                    onShouldStartLoadWithRequest={(e) => {
                        var scheme = e.url.split('://')[0]
                        if(scheme === 'http' || scheme === 'https'){
                        return true
                        }
                        return false
                    }}
                />
            </View>
        );
    }

    doBack() {
        var webView = this.refs.webView;
        if (this.state.canGoBack) {
            webView.goBack();
        }
        else {
            this.props.navigation.goBack();
        }
    }

    onNavigationStateChange(navState) {
        var {canGoBack} = navState;
        this.setState({
            canGoBack
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    backBtn: {
        marginTop: 25,
        marginLeft: 15
    },

    backIcon: {
        fontSize: 23,
        color: '#fff'
    },

    name: {
        fontSize: 17,
        color: 'white',
        marginTop: 27,
        marginLeft: 15,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    }

});