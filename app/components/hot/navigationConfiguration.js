/**
 * Created by lijizhuang on 2017/7/23.
 */
'use strict';

import { StackNavigator } from 'react-navigation'

// Screens
import HotIndex from './HotIndex'
import SearchResultsFromHots from '../home/SearchResults'
import SearchItemDetailFromHots from '../home/SearchItemDetail'

const routeConfiguration = {
    HotIndex: {screen: HotIndex},
    SearchResultsFromHots: {screen: SearchResultsFromHots},
    SearchItemDetailFromHots: {screen: SearchItemDetailFromHots}
};

// going to disable the header for now
const stackNavigatorConfiguration = {
    headerMode: 'none',
    initialRouteName: 'HotIndex',
    mode: 'card',
    transitionConfig: () => ({
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps;
            const { index } = scene;

            const translateX = position.interpolate({
                inputRange: [ index - 1, index, index + 1 ],
                outputRange: [ layout.initWidth, 0, 0 ]
            });

            const opacity = position.interpolate({
                inputRange: [ index - 1, index - 0.99, index, index + 0.99, index + 1 ],
                outputRange: [ 0, 1, 1, 0, 0 ]
            });

            return { opacity, transform: [{ translateX }] };
        }
    }) //安卓和iOS push transition保持一致
};

export const NavigatorHot = StackNavigator(routeConfiguration, stackNavigatorConfiguration);