/**
 * Created by lijizhuang on 2017/7/24.
 */
'use strict'

// React
import React from 'react'
import {Image} from 'react-native'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { NavigatorHot } from './navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        navigationState: state.tabHot
    }
};

class HotNavigation extends React.Component {
    static navigationOptions =  ({navigation, screenProps}) => ({
        tabBarLabel: '热门',
        tabBarIcon: ({tintColor}) => (<Image source={require('../../images/icon_tabbar_hotpage.png')} style={[{tintColor: tintColor},{width:25},{height:25}]}/>),
        tabBarVisible: navigation.state.params && navigation.state.params.hideTabBar  ? false : true
    });

    render(){
        const { navigationState, dispatch, navigation } = this.props;
        const {screenProps} = this.props;
        return (
            <NavigatorHot
                navigation={
                  addNavigationHelpers({
                    dispatch: dispatch,
                    state: navigationState
                  })
                }
                screenProps={screenProps}
            />
        )
    }
}
export default connect(mapStateToProps)(HotNavigation);