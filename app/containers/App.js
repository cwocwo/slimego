/**
 * Created by lijizhuang on 2017/7/6.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import applicationActions from '../actions/application'
import homeActions from '../actions/home'

import Home from './Home'

export class App extends Component {

    constructor(props) {
        super(props);
        const {application} = props;
        this.state = {
            tab: application.tab
        }
    }

    componentWillReceiveProps(props) {
        const {application} = props;
        this.setState({
            tab: application.tab //默认是application.tab 即home
        })
    }

    render() {
        const {tab} = this.state;
        const {home, homeActions} = this.props;

        return (
            <View style={styles.container}>
                {tab === 'home' &&
                    <Home {...home} actions={homeActions}/>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

App.propTypes = {
    home: PropTypes.object,
    homeActions: PropTypes.object,
};

//三.从 react-redux 获取 connect 连接组件，通过 connect(state=>{}, dispatch=>{})(App) 连接 store 和 App 容器组件。 这里的 state 是 Connect 的组件的
//state=>{} 是一个函数，他能接收到一个 state 参数，这个就是 store 里面的 state，然后通过这个函数的处理，返回一个对象，把对象里面的参数以属性传送给 App，以及附带一个 dispatch={}函数。
export default connect(state => {
    return {
        application: state.application,
        home: {
            application: state.application,
            home: state.home,
        }
    }
}, dispatch => {
    return {
        homeActions: bindActionCreators(Object.assign({}, applicationActions, homeActions), dispatch)
    }
})(App);


