/**
 * Created by lijizhuang on 2017/7/6.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Platform,
    Image,
} from 'react-native';

import {
    Navigator
} from 'react-native-deprecated-custom-components'

import SearchIndex from '../components/home/SearchIndex';
import HotIndex from '../components/hot/HotIndex';
import ArticleIndex from '../components/article/ArticleIndex';
import TabNavigator from 'react-native-tab-navigator';

export default class Home extends Component {

    constructor(props) {
        super(props);
        const {application} = props;
        this.state = {
            selectedTab: application.tab //默认是application.tab 即home
        };
    }

    componentWillReceiveProps(props) {
        const {application} = props;
    }

    componentDidMount() {
        this.mount = true;
    }

    componentWillUnmount() {
        this.mount = false
    }

    render() {
        return (
            <TabNavigator>
                {/*--首页--*/}
                {this.renderTabBarItem('首页', 'icon_tabbar_homepage', 'icon_tabbar_homepage_selected','home', '首页', SearchIndex)}
                {/*--热门--*/}
                {this.renderTabBarItem('热门', 'icon_tabbar_hotpage', 'icon_tabbar_hotpage_selected','hot', '热门', HotIndex)}
                {/*--文章--*/}
                {this.renderTabBarItem('文章', 'icon_tabbar_articlepage', 'icon_tabbar_articlepage_selected','article', '文章', ArticleIndex)}
            </TabNavigator>
        )
    }

    // 每一个TabBarItem
    renderTabBarItem(title, iconName, selectedIconName, selectedTab, componentName, component, badgeText){
        return(
            <TabNavigator.Item
                title={title}
                renderIcon={() => <Image source={{uri: iconName}} style={styles.iconStyle}/>} // 图标
                renderSelectedIcon={() =><Image source={{uri: selectedIconName}} style={styles.iconStyle}/>}   // 选中的图标
                onPress={this.switchTab.bind(this, selectedTab)}
                selected={this.state.selectedTab === selectedTab}
                selectedTitleStyle={styles.selectedTitleStyle}
                badgeText = {badgeText}
            >
                <Navigator
                    initialRoute={{name:componentName,component:component}}
                    configureScene={()=>{
                                     return Navigator.SceneConfigs.PushFromRight;
                        }}
                    renderScene={(route,navigator)=>{
                           let Component = route.component;
                           return <Component {...route.passProps} navigator={navigator} {...this.props}/>;
                        }}
                />
            </TabNavigator.Item>
        )
    }

    switchTab(tab) {
        this.setState({selectedTab: tab});
    }
}

Home.propTypes = {
    home: PropTypes.object,
    actions: PropTypes.object
};

const styles = StyleSheet.create({
    iconStyle: {
        width: Platform.OS === 'ios' ? 30 : 25,
        height: Platform.OS === 'ios' ? 30 : 25
    },

    selectedTitleStyle: {
        color: '#1C9DD7'
    }
});
